# Организация функционального и интеграцонного тестирования

## Вступление
Статья содержит описание организация тестирования продукта на стадии разработки с последующим мониторингом на стадии эксплуатации.

## Определения
+ Foe - зона контролируемая пользователем;

## Общие требования к системе тестирования
1. Тесты должны разрабатываться параллельно с разработкой функционала.
0. Тесты должны разрабатываться непосредственно разработчиками с примененимем используемых средств.
0. Тесты должны иметь возможность объединения для последующего функциоанльного и интеграционного тестирования.

Перечисленные требования направлены на снижение накладных затрат на создание системы тестирования без потери фукнциональности.

![Рассматриваемаые варианты](https://gitlab.com/catlair/scraps/-/blob/master/images/TimeZoneFrienOrFoe.jpg)

## Концепция
1. Тест представляет из себя изолированное приложение, запускаемое через CLI.
0. Тест возвращает в стандартизированном виде результат исполнения в STDOUT.
0. Тест имеют возможность каскадных вызовов, те один тест может запускать другой используя договоренности.
0. Результат теста является основанием для принятия решения о его зачете или отказе.

Концепция полностью удовлетворяет и реализует общие требования системы тестирования.

## Требование

Приложение является тестом при соблюдении требования:

Тест возвращает последней строкой структуру JSON:
{ "id":"TestID", "code":"CodeMessage", "message":"Information about code" }

## Рекомендации
1. Родительский тест обрабатывает сообщение на основании правил
- code дочернего теста не являясь пустм устанавливается как результат работы родительского теста, если он явно не изменяет его;
- постое сообщение code позволяет продолжать тест вплоть до его завершения
0. Все родительские тесты обладают возможностью пропускать через себя параметры CLI, передавая их в дочерние тесты. Таким образом дочерние тесты могут использовать параметры из верхних уровней.
0. Тесты придерживают общих принципов управления поведением с использованием ключей:
- --help - выво информации о тесте
- --exec - запуск теста
- --log - направление вывода в файл

